*Univerza Ljubljana, 2016
*Jan Blatnik, 63130013
*Projekt pri predmetu 'Računalniški vid v praksi'
*LPRecognizer: Mobilna aplikacija za prepoznavanje znakov na registrski tablici s pomočjo vgrajene kamere na mobilni napravi
 
   >>>>  POMEMBNO  <<<<<
[WARNING] Ob prvem kliku na gumb 'Recognize' se aplikacija nekaj časa ne odziva - POTREBNO POČAKATI, nato deluje normalno
Ko se aplikacija naloži (se ustvari MainActivity), datoteki classification.xml in images.xml (potrebni za KNN algoritem) še
nista naloženi. Ob prvem kliku na 'Recognize' se te dve datoteki odpreta, prebereta in naložita v aplikacijo. Ta postopek lahko
traja od 1 do 2 minuti in v tem času aplikacija ni odzivna. Ko se datoteki naložita, gumb 'Recognize' deluje normalno in brez
zakasnitev - dokler se aplikacija ne zapre.

[BUG] V nekaterih primerih (če je zaznan zgolj en kandidat za tablico) je potrebno ponovno pritisniti 'Next' preden lahko
 algoritem zazna znake na tablici ('Recognize')

[Oblika kode]
Aplikacija je napisana za Android naprave (sdk verzija 23, minimalna zahtevana verzija 19) v jeziku Java s pomočjo OpenCV knjižnic.
Ker je napisana v Javi, upoštevanje dobre prakse jezika C++ ni primerno. Upoštevana je dobra praksa Jave (Java Style Guide) in
Androida(Java Language Rules):
 - Java: https://google.github.io/styleguide/javaguide.html
 - Android: https://source.android.com/source/code-style.html
Koda je bila popravljena oziroma 'stilsko' urejena brez pomoči temu namenjenih programov.
Po pravilih Android oziroma Java dobre prakse so VSI razredi, metode in ključni deli kode komentirani z opisom oziroma razlago.   

[Opis strukture projekta]
Datoteki images.xml, ki vsebuje testne slike znakov in classifications.xml, ki testni sliki priredi vrednost znaka,
se nahajata v mapi ~\OpenCV_Test\app\src\main\assets\

Glavna koda se nahaja v mapi ~\OpenCV_Test\app\src\main\java\com\example\janbl\lprecognizer\
	Datoteka MainActivity.java predstavlja funckionalnost oziroma delovanje aplikacije in logiko algoritma.
	Datoteka CharacterContour.java je razred, ki predstavlja obris(Contour) z podatki: koordinate točk, očrtan pravokotnik
 		in ploščina. Vsebuje tudi metodo, ki preveri ali ima obris lastnosti znaka na tablici.
	Datoteka ContourComparator.java vsebuje razred, ki predstavlja metodo za primerjanje dveh objektov CharacterContour.

V mapi ~\OpenCV_Test\app je datoteka build.gradle, ki vsebuje konfiguracijo aplikacije (potrebne sdk verzije).

V mapi ~\OpenCV_Test\openCVLibrary310 so OpenCV knjižnice za Javo.