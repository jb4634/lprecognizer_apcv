package com.example.janbl.lprecognizer;

/*
 * Created by janbl on 17. 06. 2016.
 */

import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;

/**
 *  Predstavlja obris (contour) potencialnega znaka na tablici z dodatnimi podatki
 */

public class CharacterContour {
    private MatOfPoint mContourPoints;  // točke, ki sestavljajo obris
    private Rect mBoundingRect;  // kvadrat ki obdaja obris
    private double mContourArea;  // površina obrisa

    /** Constructors */
    public CharacterContour() {}
    public CharacterContour(MatOfPoint contourPoints, Rect boundingRect, double contourArea) {
        this.mContourPoints = contourPoints;
        this.mBoundingRect = boundingRect;
        this.mContourArea = contourArea;
    }

    /** preveri ali obris ustreza pogojem, ki veljajo za znak na tablici - vrne 'true' oz. 'false' */
    boolean checkIfContourIsValid() {
        boolean mValid=false;
        if (    mContourArea > 400 && mContourArea < 1700  // površina ustreza mejam
                && mBoundingRect.width / (double) mBoundingRect.height > 0.7 // razmerje ustreza mejam
                && mBoundingRect.width / (double) mBoundingRect.height < 1.5
                && mBoundingRect.width > 35 && mBoundingRect.width < 75  // širina ustreza mejam
                && mBoundingRect.height > 35 && mBoundingRect.height < 65  // višina ustreza mejam
            ) {
            mValid = true;
        }
        return mValid;
    }

    /** Getters */
    public MatOfPoint getContourPoints(){
        return this.mContourPoints;
    }
    public Rect getBoundingRect(){
        return this.mBoundingRect;
    }
    public double getContourArea(){
        return this.mContourArea;
    }
    /** Setters */
    public void setmContourPoints(MatOfPoint contourPoints){ this.mContourPoints = contourPoints; }
    public void setmBoundingRect(Rect boundingRect){
        this.mBoundingRect = boundingRect;
    }
    public void setmContourArea(double contourArea){
       this.mContourArea = contourArea;
    }
}
