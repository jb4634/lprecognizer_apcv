package com.example.janbl.lprecognizer;


import java.util.Comparator;

/**
 *  Primerjalnik (Comparator), ki omogoča primerjanje dveh objektov CharacterContour na podlagi x koordinate najbolj levega roba Contour-ja
 */
class ContourComparator implements Comparator<CharacterContour> {
    @Override
    public int compare(CharacterContour c1, CharacterContour c2) {
        if (c1.getBoundingRect().x < c2.getBoundingRect().x) return -1;
        else return 1;
    }
}