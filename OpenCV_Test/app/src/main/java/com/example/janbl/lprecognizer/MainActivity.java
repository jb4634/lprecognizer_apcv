/**
 * Jan Blatnik
 * Univerza Ljubljana, 2016
 * Projekt pri predmetu 'Računalniški vid v praksi'
 *
 * LPRecognizer:
 * Mobilna aplikacija, ki s pomočjo kamere na mobilni napravi omogoča zaznavanje registrske tablice
 * in razpoznavanje znakov na tablici, ki jih izpiše na zaslon.
 */

package com.example.janbl.lprecognizer;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt4;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.ml.KNearest;
import org.opencv.ml.Ml;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Glavna aktivnost, ki se ustvari ob zagonu aplikacije in se izvaja do zaustavitve aplikacije.
 * Pripravi okolje in lastnosti pri katerih bo aplikacija delovala in omogoči gradnike.
 * Zažene kamero in zajete slike prikazuje na zaslonu. Poveže gradnike(gumbe) z ustreznimi
 * funckijami:
 *  - 'Check4Plates' poišče slike oz. kandidate, ki lahko predstavljajo registrske tablice
 *  - 'Next' prikaže naslednjega možnega kandidata v seznamu
 *  - 'Recognize' prepozna znake na izbranem kandidatu in jih izpiše kot niz na zaslonu
 */

public class MainActivity extends AppCompatActivity implements CvCameraViewListener2 {

    /** Zabeleži novo instanco MainActivity */
    public MainActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /** Spremenljivka za logiranje sporočil ob uspehu ali napaki */
    private static final String TAG = "LPRecog::MainActivity";

    /** Omogoča nalaganje in gledanje pogleda kamere skozi OpenCV */
    private CameraBridgeViewBase mOpenCvCameraView;

    /** Matrika, ki predstavlja sliko s kamere in spremenljivke za obračanje prikazne slike */
    Mat mInputFrameRgba;
    Mat mInputFrameRgbaTranspoze;
    Mat mInputFrameRgbaResize;

    Mat mClassMat;  // Matrika razredov pri testnih slikah za KNN algoritem
    Mat mTrainImagesMat;  // Matrika testnih slik za KNN algoritem
    List<Mat> mPlatesList = null;  // Seznam matrik(slik) potencialnih tablic (kandidati)
    int mCounter = 0;  // Števec, ki določa indeks trenutno izbranega kandidata (iz 'mPlatesList')

    /** Naloži OpenCV in mu omogoči kamero */
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    /**
     * Ko se ustvari aplikacija pripravi okolje:
     *  1.) naloži gradnike
     *  2.) omogoči prikaz kamere
     *  3.) nastavi poslušalce za posamezen gumb:
     *      - 'Check4Plates':  poišče kandidate za registrsko tablico s findPlate(slika)
     *      - 'Next': prikaže naslednjega kandidata
     *      - 'Recognize': prepozna znake na sliki s charRecognition(kandidat) in izpiše na zaslon)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");  // Zabeleži, da se je sprožil 'onCreate'

        /** Naloži okolje in gradnike */
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.show_camera);

        /** Omogoči prikaz kamere in nastavi listener */
        mOpenCvCameraView = (JavaCameraView) findViewById(R.id.javaCameraView);
        if (mOpenCvCameraView != null) {
            mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
            mOpenCvCameraView.setCvCameraViewListener(this);
        }

        /** Gradnikom za gumbe in pogled besedila priredi spremenljivke */
        final Button mBtnRecognize = (Button) findViewById(R.id.btn_recognize);
        final Button mBtnCheck = (Button) findViewById(R.id.btn_check);
        final Button mBtnNext = (Button) findViewById(R.id.btn_next);
        final TextView mTextView = (TextView) findViewById(R.id.textView);
        mTextView.setTextColor(Color.WHITE);        // Besedilo nastavi na belo barvo
        mTextView.setTextSize(30);        // Velikost besedila na 30

        /**
         *  Nastavi listener 'onClick()' na gumb "Check4Plates": požene findPlate(mInputFrameRgba),
         * ki s trenutne slike na zaslonu poišče kandidate, ki bi lahko predstavljali tablico
         * in jih naloži v 'platesList'
         */
        if (mBtnCheck != null) {
            mBtnCheck.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mPlatesList = findPlate(mInputFrameRgba);
                    String mContent;
                    if (mPlatesList.isEmpty()) mContent = "No licence plate";
                    else mContent = "Number of plates: " + mPlatesList.size();
                    mTextView.setText(mContent);
                    mCounter =0;
                }
            });
        }

        /**
         * Nastavi listener 'onClick()' na gumb "Next": Na zaslonu prikaže naslednjega kandidata za
         * tablico in spremeni števec 'mCounter'
         */
        if (mBtnNext != null) {
            mBtnNext.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mBtnNext.setText("Next "+ mCounter);
                    mTextView.setText("Loading");
                    String mContent="";
                    if(mPlatesList.isEmpty()) mTextView.setText("No licence plate");
                    else if( mCounter < mPlatesList.size()){
                        /** Če list ni prazen prikaže na 'imageView' naslednjega kandidata */
                        if(!mPlatesList.get(mCounter).empty()) {
                            ImageView mImageView = (ImageView) findViewById(R.id.imageView);
                            Mat mCurrentMat;
                            if(mCounter==0) mCurrentMat = mPlatesList.get(mPlatesList.size()-1);
                            else mCurrentMat = mPlatesList.get(mCounter-1);
                            Bitmap mCurrentBitMap = Bitmap.createBitmap(mCurrentMat.cols(),
                                    mCurrentMat.rows(),Bitmap.Config.ARGB_8888);
                            Utils.matToBitmap(mCurrentMat, mCurrentBitMap);
                            mImageView.setImageBitmap(mCurrentBitMap);
                        }
                        mCounter++;
                    }
                    else{
                        /** Števec je prišel do konca seznama in se resetira */
                        mCounter = 0;
                    }
                    mTextView.setText(mContent);

                }
            });
        }

        /**
         *  Nastavi listener 'onClick()' na gumb "Recognize": Nad trenutnim kandidatom požene
         * algoritem za prepoznavanje znakov charRecognition(trenutni) in jih združi v string
         * ter prikaže na zaslonu
         */
        if (mBtnRecognize != null) {
            mBtnRecognize.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String mContent="";
                    mTextView.setText("Loading");
                    if(mPlatesList.isEmpty()) mTextView.setText("No licence plate");
                    else if( mCounter < mPlatesList.size()) {
                        if (!mPlatesList.get(mCounter).empty()) {
                           /** Ustrezno spremeni velikost kandidata za nadaljno obdelavo */
                            Mat mResizedMat = new Mat();
                            Imgproc.resize(mPlatesList.get(mCounter), mResizedMat, new Size(600,
                                    ((double) (mPlatesList.get(mCounter).rows()) /
                                            mPlatesList.get(mCounter).cols())*600 ));
                            mContent = charRecognition(mResizedMat); //zažene prepoznavanje znakov
                        }
                    }
                    mTextView.setText(mContent);
                }
            });
        }
    }

    /** Ob pavziranju aplikacije se tudi onemogoči pogled kamere */
    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    /* Ob nadaljevanju aplikacije se ponovno naloži in omogoči komponente ter zabeleži uspešnost */
    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found.Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    /** Ob zaustavitvi aplikacije se onemogoči tudi pogled kamere */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    /*Ob zagonu pogleda kamere(CameraView) se nastavijo spremenljivke matrik na ustrezne velikosti*/
    public void onCameraViewStarted(int width, int height) {
        mInputFrameRgba = new Mat(height, width, CvType.CV_8UC4);
        mInputFrameRgbaResize = new Mat(height, width, CvType.CV_8UC4);
        mInputFrameRgbaTranspoze = new Mat(width, width, CvType.CV_8UC4);
    }

    /* Ob zaustavitvi pogleda kamere se sprosti spremenljivko za trenutno prikazano sliko s kamere*/
    public void onCameraViewStopped() {
        mInputFrameRgba.release();
    }

    /** Ob vsakem zajemu nove slike iz kamere jo ustrezno obrne in vrne za prikaz na zaslonu  */
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        mInputFrameRgba = inputFrame.rgba();

        /** Obrne sliko za 90 stopinj za bolj primeren prikaz */
        Core.transpose(mInputFrameRgba, mInputFrameRgbaTranspoze);
        Imgproc.resize(mInputFrameRgbaTranspoze, mInputFrameRgbaResize,
                mInputFrameRgbaResize.size(), 0, 0, 0);
        Core.flip(mInputFrameRgbaResize, mInputFrameRgba, 1);

        return mInputFrameRgba; // vrne ustrezno obrnjeno sliko za posodobljen prikaz
    }

    /**
     *  Metoda List<Mat> findPlates(Mat img) na sliki poišče vse obrise, ki lahko predstavljajo
     * registrsko tablico in jih vrne kot seznam matrik.
     * Vhodno sliko 'img' pretvori v sivinsko, jo zamegli, poudari robove in poišče vse obrise.
     * Med vsemi obrisi preišče tiste, ki nakazujejo na tablico, jih shrani v seznam matrik in
     * vrne kot rezultat metode.Pogoji so razmerje širine in višine stranice očrtanega pravokotnika
     * med 0.2 in 0.7 ter ploščina izrezane slike več kot 10000.
     *
     * @param img matrika (slika), ki naj bi vsebovala registrsko tablico.
     * @return    vrne seznam kandidatov slik, ki predstavlajo registrske tablice
     */
    private List<Mat> findPlate(Mat img) {

        /**
         *  Definira pomožne spremenljivke za filtriranje in sliko pretvori v sivinsko, jo zamegli
         * in poudari robove
         */
        Mat mImgGray = new Mat();
        Mat mImgBlur = new Mat();
        Mat mImgCanny = new Mat();
        Imgproc.cvtColor(img, mImgGray,  Imgproc.COLOR_BGR2GRAY);    // Sivinska slika
        Imgproc.blur(mImgGray, mImgBlur,new Size(3,3));              // Zamegljena slika
        Imgproc.Canny(mImgBlur, mImgCanny, 100, 200, 3, false);      // Poudarjeni robovi

        /** Definira spremenljivke, ki bodo vsebovale obrise in hierarhijo, ter poišče vse obrise*/
        List<MatOfPoint> mContours = new ArrayList<>();
        MatOfInt4 mHierarchy = new MatOfInt4();
        Imgproc.findContours(mImgCanny, mContours, mHierarchy, Imgproc.RETR_TREE,
                Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));

        /** Iterira skozi vse obrise,okoli njih oriše pravokotnik in preveri,če ustrezajo pogojem*/
        Mat mImgResized;        // Sliko, ki predstavlja tablico
        List<Mat> mPotentialPlates = new ArrayList<>();        // Kandidati za tablico
        for( int i = 0; i < mContours.size(); i++ ){

            /**
             * Trenutni obris pretvori v matriko točk, okoli njih obda najmanjši možni pravokotnik
             * in izračuna razmerje med širino in višino stranice
             */
            MatOfPoint2f mTmpMp2f = new MatOfPoint2f(mContours.get(i).toArray());
            RotatedRect mRotRect = Imgproc.minAreaRect(mTmpMp2f);
            double mWhRatio = mRotRect.size.width /mRotRect.size.height;

            /**
             *  Preveri,če ustreza podatkom,pretvori pravokotnik v Rect,izreže območje pravokotnika
             * iz začetne slike ('img'), dobi sliko potencialne tablice in jo doda v seznam.
             */
            if ( 2.0 < mWhRatio && mWhRatio < 10.0){       // Preveri ali je pravo razmerje stranic
                Point[] mTmpPoints = new Point[4];
                mRotRect.points(mTmpPoints);
                Rect mTmpRect = mRotRect.boundingRect();
                try {                                 // Poskuša izrezati potencialno sliko tablice
                    mImgResized = img.submat(mTmpRect);
                }
                catch(Exception e){                   // Ob napaki preskoči trenutni obris
                    continue;
                }
                if(mImgResized.size().area()<=10000){         // Preveri ali ustreza površina slike
                    continue;
                }
                mPotentialPlates.add(mImgResized);
            }
        }
        return mPotentialPlates;       // Vrne seznam slik vseh potencialnih tablic
    }

    /**
     * Metoda String charRecognition(plate) prepozna znake na sliki tablice ('plate') s pomočjo
     * KNN algoritma in jih vrne kot niz znakov.
     * Naloži konfiguracijski datoteki classification.xml (vsebuje razrede testnih slik) in
     * images.xml (vsebuje testne slike), ki sta potrebni za KNN algoritem. Sproži učenje oziroma
     * treniranje algoritma. Sliko tablice spremeni v sivinsko, jo zamegli in spremeni v binarno.
     * Poišče vse obrise na binarni sliki, jih shrani s pripadajočimi podatki v seznam vseh
     * potencialnih obrisov znakov in za vsakega preveri ali ustreza pogojem za znak. Če ustreza,
     * se obris s podatki doda v seznam vseh veljavnih obrisov. Tega se nato uredi od najbolj
     * levega do najbolj desnega znaka nato pa po vrsti testira s prej naučenim KNN algoritmom.
     * Znake dodaja v niz, ki ga vrne kot rezultat metode.
     *
     * @param plate Slika oziroma matrika registerske tablice
     * @return      Vrne niz znakov na tablici
     */
    String charRecognition(Mat plate){

        /** Če ni slike tablice oziroma je ta prazna, vrne '-1' - ponazarja napako */
        if(plate == null || plate.empty()){
            return "-1";
        }

        /**
         *  Če datoteka še ni naložena v matriko, se s pomočjo MyFileReader odpre
         * classifications.xml (razrede testnih slik), jih prebere, pretvori in naloži v matriko
         * mClassMat -> potrebna za treniranje KNN algoritma
         */
        if(mClassMat == null) {
            MyFileReader mFileReader = new MyFileReader();
            mFileReader.open("classifications.xml");        // Odpre datoteko in shrani v Document

            /** Prebere vrednosti, jih pretvori in shrani v matriko mClassMat */
            mClassMat = mFileReader.readMat("classifications");

            /** Ob neuspešnem branju zabeleži opozorilo v log */
            if (mClassMat == null) {
                Log.w(TAG,"Napaka pri branju classifications.xml");
                return "-1";
            }
            mFileReader.close(); // Zapre mFileReader (Document nastavi na null)
        }

        /**
         *  Če datoteka še ni naložena v matriko, se s pomočjo MyFileReader odpre
         * images.xml (testne slike), jih prebere, pretvori in naloži v matriko
         * mTrainImagesMat -> potrebna za treniranje KNN algoritma
         */
        if(mTrainImagesMat == null) {
            MyFileReader mFileReader = new MyFileReader();
            mFileReader.open("images.xml");        // Odpre datoteko in shrani v Document

           /** Prebere vrednosti, jih pretvori in shrani v matriko mTrainImagesMat */
            mTrainImagesMat = mFileReader.readMat("images");

            /** Ob neuspešnem branju zabeleži opozorilo v log */
            if (mTrainImagesMat == null) {
                Log.w(TAG,"Napaka pri branju images.xml");
                return "-1";
            }
            mFileReader.close(); // Zapre mFileReader (Document nastavi na null)
        }

        /**
         *  Ustvari novo instanco KNearest (KNN algoritma) in ga nauči/natrenira s pomočjo
         * prebranih datotek/matrik mTrainImagesMat in mClassMat
         */
        KNearest mKnn = KNearest.create();
        mKnn.train(mTrainImagesMat, Ml.ROW_SAMPLE, mClassMat);

        /** Pomožne spremenljivke za ustrezno pretvorbo matrike tablice za iskanje obrob */
        Mat mBlurredMat = new Mat();
        Mat mThreshMat = new Mat();
        Mat mThreshClone;

        /** Če ima več kot 2 kanala jo pretvori v sivinsko sliko */
        if (plate.channels() > 2) Imgproc.cvtColor(plate, plate, Imgproc.COLOR_BGR2GRAY);

        /** Zamegli sliko */
        Imgproc.GaussianBlur(plate, mBlurredMat, new Size(5, 5), 0);

        /** Sivinsko sliko s pomočjo mejne vrednosti(threshold) spremeni binarno (črno belo) */
        Imgproc.adaptiveThreshold(mBlurredMat, mThreshMat, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                Imgproc.THRESH_BINARY_INV, 11, 2);

        /** Ustvari spremenljivke potrebne za iskanje obrisov in klonira binarno sliko*/
        List<MatOfPoint> mContours = new ArrayList<>();        // seznam dobljenih obrisov
        MatOfInt4 mHierarchy = new MatOfInt4();        // matrika hierarhije obrisov
        mThreshClone = mThreshMat.clone();        // nova kopija - iskanje obrisov spremeni sliko

        /** Poišče vse obrise na sliki */
        Imgproc.findContours(mThreshClone ,mContours, mHierarchy, Imgproc.RETR_TREE,
                Imgproc.CHAIN_APPROX_SIMPLE);

        /**
         * Določi spremenljivki za seznam vseh potencialnih obrisov in seznam veljavnih obrisov
         * znakov. Iterira skozi vse obrise in za vsakega shrani potrebne podatke (točke,
         * pravokotnik in ploščino) v objekt CharacterContour ter ga doda v seznam vseh
         * potencialnih obrisov
         */
        List<CharacterContour> mAllCharContourList = new ArrayList<>();//seznam vseh obrisov znakov
        List<CharacterContour> mValidCharContourList = new ArrayList<>(); //seznam veljavnih znakov

        /** Iterira skozi vse obrise in ga s pripadajočimi podatki shrani v seznam vseh obrisov */
        for (int i = 0; i < mContours.size(); i++) {
            MatOfPoint mContourTmp = mContours.get(i);
            CharacterContour mCharContour = new CharacterContour(mContourTmp,
                    Imgproc.boundingRect(mContourTmp), Imgproc.contourArea(mContourTmp));
            mAllCharContourList.add(mCharContour);
        }

        /**
         *  Naredi kopijo slike tablice katero se prikazuje na zaslonu. Iterira skozi seznam vseh
         * obrisov potencialnih znakov in z vsakega preveri ali ustreza pogojem za veljaven znak
         * Če ustreza ga doda v seznam veljavnih obrisov znakov in doda pravokotnik na kopijo slike
         */
        Mat mPlateClone = plate.clone();
        for (int i = 0; i < mAllCharContourList.size(); i++) {
            if (mAllCharContourList.get(i).checkIfContourIsValid()) {
                mValidCharContourList.add(mAllCharContourList.get(i));
                Imgproc.rectangle(mPlateClone, mAllCharContourList.get(i).getBoundingRect().tl(),
                        mAllCharContourList.get(i).getBoundingRect().br(), new Scalar(0,0,255));
            }
        }

        /** Uredi obrise od levega proti desni in definira spremenljivko za prikaz znakov tablice*/
        Collections.sort(mValidCharContourList,new ContourComparator());
        String mPlateString = "";

        /**
         * Iterira skozi vse veljavne obrise znakov, vsakega posameznega spremeni v sliko s
         * pomočjo točk očrtanega pravokotnika. Sliko spremeni v primerno velikost za testiranje
         * s prej natreniranim KNN algoritmom (mKnn), testira in dobljen rezultat pretvori v
         * Character in ga doda v besedni niz mPlateString
         */
        for (int i = 0; i < mValidCharContourList.size(); i++) {

            /** Izreže(crop) sliko s pomočjo točk očrtanega pravokotnika */
            Mat mCharMat = new Mat(mThreshMat, mValidCharContourList.get(i).getBoundingRect());
            Mat mCharResizedMat = new Mat();
            Imgproc.resize(mCharMat, mCharResizedMat, new Size(20, 30)); // spremeni velikost 20x30

            /** Pretvori izrezano Mat(sliko znaka) v Mat predstavljeno s float vrednostmi */
            Mat mCharFloatMat = new Mat();
            mCharResizedMat.convertTo(mCharFloatMat, CvType.CV_32FC1);

            /**
             *  Definira pomožne spremenljivke in nadaljne oblikuje float zapis vrednosti Mat,
             * ki so potrebne za izvedbo testa KNN algoritma
             */
            Mat mCharFlattenedFloatMat  = mCharFloatMat.reshape(1, 1);
            Mat mCurrentCharMat = new Mat(0, 0, CvType.CV_32F);

            /**
             *  Izvede naučen/natreniran KNN algoritem nad izrezano in ustrezno preoblikovano
             *  matriko, ki vrne float obliko kode iskanega znaka
             */
            float mCharCodeFloat = mKnn.findNearest(mCharFlattenedFloatMat, 1, mCurrentCharMat);

            /** Float številko pretvori v char in ga doda v niz za prikaz znakov na tablici */
            mPlateString = mPlateString + (char)((int) mCharCodeFloat);
        }

        /**
         * Definira začasne spremenljivke za izris obarvanih pravokotnikov okoli zaznanih veljavnih
         * znakov tablice. Ustvari primeren Bitmap in v njega pretvori sliko tablice s pravokotniki
         */
        Bitmap mBitMap = Bitmap.createBitmap(mPlateClone.cols(), mPlateClone.rows(),
                Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mPlateClone, mBitMap);

        /** Poišče gradnik, ki prikazuje manjšo sliko na zaslonu in na njem izriše trenutno sliko*/
        ImageView mImgView = (ImageView) findViewById(R.id.imageView);
        mImgView.setImageBitmap(mBitMap);

        return mPlateString; // vrne niz, ki predstavlja niz znakov na tablici
    }

    /**
     * Razred služi poenostavljenemu odpiranju datotek za branje in pretvarjanju (parsanju)
     * xml datoteke v matriko.
     */
    class MyFileReader {

        /** Spremenljivka s parsanimi podatki o odprti datoteki*/
        private Document mDocument;

        /** Konstruktor - nastavi mDocument na 'null' */
        public MyFileReader() {
            mDocument = null;
        }

        /**
         * Odpre datoteko za branje v InputStream, parsa datoteko in podatke shrani v mDocument
         * @param filePath absolutna pot od /app/src/main/assets/ do željene datoteke
         */
        public void open(String filePath) {
            try {
                /** Odpre datoteko v InputStream */
                InputStream mInputStream = getAssets().open(filePath);
                if( mInputStream == null) Log.w(TAG, "Can not open file: "+filePath );
                else {
                    /** Iz InputStream parsa vsebino v obliko Document */
                    mDocument = DocumentBuilderFactory.newInstance()
                                    .newDocumentBuilder().parse(mInputStream);
                    mDocument.getDocumentElement().normalize();
                }
                if(mInputStream !=null) mInputStream.close();  //zapre input stream
            } catch(Exception e) {    //zabeleži izjemo
                Log.w(TAG,"Excpetion while openning file: "+filePath);
            }
        }

        /**
         * mDocument preoblikuje v ustrezen tip (int ali float) matrike na podlagi podatkov
         * @param tag ime glavnega elementa
         */
        public Mat readMat(String tag) {
            Mat mReadMat = null;        //matrika, ki jo vrne
            NodeList mNodelist;         //seznam vozlišč, ki jih vsebuje glavni element
            if(mDocument !=null) {
                mNodelist = mDocument.getElementsByTagName(tag);
            }
            else{
                Log.w(TAG,"mDocument is null");
                return null;
            }
           /**Iterira skozi vsa vozlišča v seznamu mNodeList in prebrane podatke shrani v matriko*/
            for( int i = 0 ; i<mNodelist.getLength() ; i++ ) {
                Node mNode = mNodelist.item(i);
                if( mNode.getNodeType() == Node.ELEMENT_NODE ) {
                    org.w3c.dom.Element mElement = (org.w3c.dom.Element)mNode;

                    /** Preveri ali datoteka predstavlja željeno vrsto matrike */
                    String mType_id = mElement.getAttribute("type_id");
                    if( "opencv-matrix".equals(mType_id) == false) {
                        Log.w(TAG,"Fault type_id ");
                    }

                   /** V spremenljivke shrani vrednosti izbranih elementov */
                    String mRowsStr =mElement.getElementsByTagName("rows").item(0).getTextContent();
                    String mColsStr =mElement.getElementsByTagName("cols").item(0).getTextContent();
                    String mDtStr = mElement.getElementsByTagName("dt").item(0).getTextContent();
                    String mDataStr =mElement.getElementsByTagName("data").item(0).getTextContent();

                    int mRows = Integer.parseInt(mRowsStr);
                    int mCols = Integer.parseInt(mColsStr);
                    int mType;

                    /**
                     *  S Scanner-jem se iterira skozi vse podatke "data", jih pretvori v ustrezen
                     * tip in vstavi v ustrezno vrstico in stolpec v matriki
                     */
                    Scanner mScanner = new Scanner(mDataStr);

                    /** V kolikor so podatki tipa float */
                    if( "f".equals(mDtStr) ) {
                        mType = CvType.CV_32F;
                        mReadMat = new Mat( mRows, mCols, mType );
                        float[] mFloatData = new float[1];
                        for( int r=0 ; r<mRows ; r++ ) {
                            for( int c=0 ; c<mCols ; c++ ) {
                                if( mScanner.hasNextFloat() ) {
                                    mFloatData[0] = mScanner.nextFloat();
                                }
                                else if(mScanner.hasNext()){
                                    Object x = mScanner.next();
                                    mFloatData[0] = Float.parseFloat((String)x);
                                }
                                else {
                                    mFloatData[0] = 0;
                                }
                                mReadMat.put(r, c, mFloatData);
                            }
                        }
                    }
                    /** V kolikor so podatki tipa int */
                    else if( "i".equals(mDtStr) ) {
                        mType = CvType.CV_32S;
                        mReadMat = new Mat( mRows, mCols, mType );
                        int mIntData[] = new int[1];
                        for( int r=0 ; r<mRows ; r++ ) {
                            for( int c=0 ; c<mCols ; c++ ) {
                                if( mScanner.hasNextInt() ) {
                                    mIntData[0] = mScanner.nextInt();
                                }
                                else {
                                    mIntData[0] = 0;
                                }
                                mReadMat.put(r, c, mIntData);
                            }
                        }
                    }
                    else Log.w(TAG,"Unrecognized dt value: "+mDtStr);
                }
            }
            return mReadMat;
        }

        /** Ponastavi instanco razreda */
        public void close(){
            mDocument = null;
        }
    }
}



